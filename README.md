# cakes
A simple application for the love of cakes.  
Available to view [here](http://iamcdonald.gitlab.io/cakes/).

## commits & branches
Features were developed on branches and then merged back into `master`.
When merged, history from the feature branch was squashed to keep the `master` git history clear.

I've left the feature branches on the remote if you want to have a look through the development of each feature (although usually these would be deleted post the feature being completed).

## CI/CD
Each push to any branch results in both unit and acceptance tests being run. Furthermore, a commit on `master`, if all tests pass, results in the application being deployed to the gitlab page for the repo.

## tech choices
- __application code__: React, Rematch, SCSS
- __build__: Babel, Webpack, PostCSS
- __test__: ava, testdouble, enzyme, cypress
- __lint__: semistandard, prettier, jsx-a11y (accessibility checks)

## running
- `npm install` - install dependencies
- `npm run lint` - run linting accross src/test
- `npm run test:unit` - run unit tests
- `npm run test:coverage` - run unit tests with coverage
- `npm run test:acceptance` - run acceptance tests
- `npm run build` - build application
- `npm run start:dev` - start webpack dev server
- `npm run public:start` - serve built app
- `npm run public:stop` - stop serving built app
