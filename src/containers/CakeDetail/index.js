import { connect } from 'react-redux';
import { CakeDetail } from '../../components';

export const mapState = ({ cakes }, props) => ({
  cake: cakes.list.find(cake => cake.id === props.cakeId)
});

export default connect(mapState)(CakeDetail);
