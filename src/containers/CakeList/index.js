import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CakeList, Loader, ErrorBox } from '../../components';

export class CakeListContainer extends Component {
  componentDidMount() {
    const { getCakes, cakes } = this.props;
    if (!cakes) {
      getCakes();
    }
  }
  getContents() {
    const { cakes, pending, failed } = this.props;
    if (pending) {
      return <Loader />;
    }
    if (failed) {
      return <ErrorBox>An error has occurred fetching the cakes.</ErrorBox>;
    }
    if (cakes) {
      return <CakeList cakes={cakes} />;
    }
    return null;
  }
  render() {
    return <div>{this.getContents()}</div>;
  }
}

CakeListContainer.propTypes = {
  getCakes: PropTypes.func.isRequired,
  cakes: PropTypes.array,
  pending: PropTypes.bool.isRequired,
  failed: PropTypes.bool.isRequired
};

export const mapState = ({ cakes }) => ({
  cakes: cakes.list,
  pending: cakes.getAll.pending,
  failed: cakes.getAll.failed
});

export const mapDispatch = ({ cakes }) => {
  return {
    getCakes: cakes.getAll
  };
};

export default connect(mapState, mapDispatch)(CakeListContainer);
