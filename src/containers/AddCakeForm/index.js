import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { AddCakeForm, ErrorBox } from '../../components';

export class AddCakeFormContainer extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }
  componentWillUpdate(nextProps) {
    const { success } = nextProps;
    if (success) {
      this.navigateToCakesPage();
    }
  }
  navigateToCakesPage() {
    this.props.history.push('/');
  }
  onSubmit(cake) {
    this.props.addCake(cake);
  }
  onCancel() {
    this.navigateToCakesPage();
  }
  render() {
    const { failed } = this.props;
    return (
      <div>
        {failed && <ErrorBox>Failed to add cake, please try again.</ErrorBox>}
        <AddCakeForm onSubmit={this.onSubmit} onCancel={this.onCancel} />
      </div>
    );
  }
}

AddCakeFormContainer.propTypes = {
  failed: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
  addCake: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired
};

export const mapState = ({ cakes }) => ({
  failed: cakes.add.failed,
  success: cakes.add.success
});

export const mapDispatch = ({ cakes }) => {
  return {
    addCake: cakes.add
  };
};

export default connect(mapState, mapDispatch)(AddCakeFormContainer);
