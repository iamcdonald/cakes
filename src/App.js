import React from 'react';
import { HashRouter as Router, Route } from 'react-router-dom';
import { Cakes, AddCake, Cake } from './pages';
import style from './app.scss';

export default () => (
  <Router basename="/cakes">
    <div className={style.container} data-test="app">
      <Route path="/" exact component={Cakes} />
      <Route path="/add" component={AddCake} />
      <Route path="/detail/:id" component={Cake} />
    </div>
  </Router>
);
