import axios from 'axios';

const CAKES_BASE_URL =
  'http://ec2-52-209-201-89.eu-west-1.compute.amazonaws.com:5000/api/cakes';

const getAll = async () => (await axios.get(CAKES_BASE_URL)).data;

const add = async cake => (await axios.post(CAKES_BASE_URL, cake)).data;

export default {
  getAll,
  add
};
