export { default as Cakes } from './Cakes';
export { default as AddCake } from './AddCake';
export { default as Cake } from './Cake';
