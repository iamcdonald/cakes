import React from 'react';
import { CakeList } from '../../containers';
import { Header } from '../../components';

const Cakes = () => (
  <div data-test="cakes-page">
    <Header title="Cakes" link={{ path: '/add', text: 'add a cake' }} />
    <CakeList />
  </div>
);

export default Cakes;
