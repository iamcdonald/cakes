import React from 'react';
import { Header } from '../../components';
import { AddCakeForm } from '../../containers';
import PropTypes from 'prop-types';

const AddCake = ({ history }) => (
  <div data-test="add-cake-page">
    <Header title="Add Cake" />
    <AddCakeForm history={history} />
  </div>
);

AddCake.propTypes = {
  history: PropTypes.object.isRequired
};

export default AddCake;
