import React from 'react';
import PropTypes from 'prop-types';
import { Header } from '../../components';
import { CakeDetail } from '../../containers';

const Cakes = ({ match }) => (
  <div data-test="cake-detail-page">
    <Header title="Cake Detail" link={{ path: '/', text: 'view all cakes' }} />
    <CakeDetail cakeId={match.params.id} />
  </div>
);

Cakes.propTypes = {
  match: PropTypes.object.isRequired
};

export default Cakes;
