import React from 'react';
import PropTypes from 'prop-types';
import style from './error-box.scss';

const ErrorBox = ({ children }) => (
  <div className={style.errorBox} data-test="error-box">
    {children}
  </div>
);

ErrorBox.propTypes = {
  children: PropTypes.string.isRequired
};

export default ErrorBox;
