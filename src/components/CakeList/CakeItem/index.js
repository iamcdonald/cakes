import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import style from './cake-item.scss';

const CakeItem = ({ cake: { imageUrl, name, id } }) => (
  <Link to={`/detail/${id}`}>
    <li className={style.item} data-test={`cake-item-${id}`}>
      <img src={imageUrl} alt="" />
      <span>{name}</span>
    </li>
  </Link>
);

CakeItem.propTypes = {
  cake: PropTypes.object.isRequired
};

export default CakeItem;
