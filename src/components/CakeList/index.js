import React from 'react';
import PropTypes from 'prop-types';
import CakeItem from './CakeItem';
import style from './cake-list.scss';

const CakeList = ({ cakes }) => (
  <ul className={style.list} data-test="cake-list">
    {cakes.map(cake => <CakeItem key={cake.id} cake={cake} />)}
  </ul>
);

CakeList.propTypes = {
  cakes: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default CakeList;
