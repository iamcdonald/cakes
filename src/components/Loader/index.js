import React from 'react';
import style from './loader.scss';

const Loader = () => (
  <div className={style.loader} data-test="loader">
    Loading...
  </div>
);

export default Loader;
