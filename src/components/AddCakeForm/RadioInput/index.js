import React from 'react';
import styles from './radio-input.scss';
import PropTypes from 'prop-types';

const getWidth = options => `${100 / options.length}%`;

const RadioInput = ({ name, text, options, onChange }) => (
  <div className={styles.radioInput}>
    <label htmlFor={name}>
      <span className={styles.label}>{text}:</span>
      <span className={styles.inputs}>
        {options.map((opt, idx) => (
          <span
            key={idx}
            className={styles.option}
            style={{ width: getWidth(options) }}
          >
            <label htmlFor={opt.value}>
              <span>{opt.value}</span>
              <input
                type="radio"
                name={name}
                value={opt.value}
                onChange={onChange}
              />
            </label>
          </span>
        ))}
      </span>
    </label>
  </div>
);

RadioInput.propTypes = {
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  onChange: PropTypes.func.isRequired
};

export default RadioInput;
