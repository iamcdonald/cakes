export const validateCake = cake =>
  Boolean(cake.name && cake.imageUrl && cake.comment && cake.yumFactor);
