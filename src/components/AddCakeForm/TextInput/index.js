import React from 'react';
import style from './text-input.scss';
import PropTypes from 'prop-types';

const TextInput = ({ name, text, onChange, type = 'text' }) => (
  <div className={style.textInput}>
    <label htmlFor={name}>
      <span className={style.label}>{text}:</span>
      <input
        name={name}
        className={style.input}
        onChange={onChange}
        type={type}
      />
    </label>
  </div>
);

TextInput.propTypes = {
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  type: PropTypes.string,
  onChange: PropTypes.func.isRequired
};

export default TextInput;
