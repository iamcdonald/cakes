import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RadioInput from './RadioInput';
import TextInput from './TextInput';
import ErrorBox from '../ErrorBox';
import { validateCake } from './validator';
import style from './add-cake-form.scss';

export const YUM_OPTIONS = [
  {
    value: 1
  },
  {
    value: 2
  },
  {
    value: 3
  },
  {
    value: 4
  },
  {
    value: 5
  }
];

class AddCakeForm extends Component {
  constructor(props) {
    super(props);
    this.onNameChange = this.onNameChange.bind(this);
    this.onCommentChange = this.onCommentChange.bind(this);
    this.onImageURLChange = this.onImageURLChange.bind(this);
    this.onYumFactorChange = this.onYumFactorChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.state = {
      cake: {},
      invalid: false
    };
  }
  addToCakeState(state) {
    this.setState({
      cake: Object.assign({}, this.state.cake, state)
    });
  }
  onNameChange(e) {
    this.addToCakeState({ name: e.currentTarget.value });
  }
  onCommentChange(e) {
    this.addToCakeState({ comment: e.currentTarget.value });
  }
  onImageURLChange(e) {
    this.addToCakeState({ imageUrl: e.currentTarget.value });
  }
  onYumFactorChange(e) {
    this.addToCakeState({ yumFactor: e.currentTarget.value });
  }
  onSubmit(e) {
    if (validateCake(this.state.cake)) {
      this.props.onSubmit(this.state.cake);
    } else {
      this.setState({ invalid: true });
    }
    e.preventDefault();
  }
  onCancel() {
    this.props.onCancel();
  }
  render() {
    const { invalid } = this.state;
    return (
      <form onSubmit={this.onSubmit} data-test="add-cake-form">
        {invalid && <ErrorBox>All fields are required.</ErrorBox>}
        <TextInput
          name="name"
          text="Name"
          type="text"
          onChange={this.onNameChange}
        />
        <TextInput
          name="comment"
          text="Comment"
          type="text"
          onChange={this.onCommentChange}
        />
        <TextInput
          name="imageUrl"
          text="Image URL"
          type="url"
          onChange={this.onImageURLChange}
        />
        <RadioInput
          name="yums"
          options={YUM_OPTIONS}
          text="Yumz?"
          onChange={this.onYumFactorChange}
        />
        <button
          type="button"
          className={style.button}
          onClick={this.onCancel}
          data-test="cancel-button"
        >
          Cancel
        </button>
        <button type="submit" className={style.button} onClick={this.onSubmit}>
          Submit
        </button>
      </form>
    );
  }
}

AddCakeForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
};

export default AddCakeForm;
