import React from 'react';
import PropTypes from 'prop-types';
import style from './cake-detail.scss';

const CakeDetail = ({ cake }) => (
  <div className={style.detail}>
    <img src={cake.imageUrl} alt="" />
    <dl data-test="cake-detail">
      <div>
        <dt>Name</dt>
        <dd>{cake.name}</dd>
      </div>
      <div>
        <dt>Comment</dt>
        <dd>{cake.comment}</dd>
      </div>
      <div>
        <dt>Yums</dt>
        <dd>{cake.yumFactor}</dd>
      </div>
    </dl>
  </div>
);

CakeDetail.propTypes = {
  cake: PropTypes.object.isRequired
};

export default CakeDetail;
