import React from 'react';
import style from './header.scss';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Header = ({ title, link }) => (
  <div className={style.header} data-test="header">
    <h1 className={link && style.withLink}>{title}</h1>
    {link && (
      <Link to={link.path} className={style.link}>
        <span data-test="header-link">{link.text}</span>
      </Link>
    )}
  </div>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
  link: PropTypes.objectOf(PropTypes.string)
};

export default Header;
