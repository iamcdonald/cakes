import { init } from '@rematch/core';
import cakes from './cakes';

const store = init({
  models: {
    cakes
  }
});

export default store;
