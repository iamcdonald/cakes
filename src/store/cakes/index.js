import { cakes as cakesService } from '../../services';

const cakes = {
  state: {
    list: null,
    getAll: {
      pending: false,
      failed: false
    },
    add: {
      pending: false,
      failed: false,
      success: false
    }
  },
  reducers: {
    getAllPending: state =>
      Object.assign({}, state, {
        list: null,
        getAll: {
          pending: true,
          failed: false
        }
      }),
    getAllSuccess: (state, cakes) =>
      Object.assign({}, state, {
        list: cakes,
        getAll: {
          pending: false,
          failed: false
        }
      }),
    getAllFailure: state =>
      Object.assign({}, state, {
        list: null,
        getAll: {
          pending: false,
          failed: true
        }
      }),
    addPending: state =>
      Object.assign({}, state, {
        add: {
          pending: true,
          failed: false,
          success: false
        }
      }),
    addSuccess: (state, cake) =>
      Object.assign({}, state, {
        list: [cake].concat(state.list),
        add: {
          pending: false,
          failed: false,
          success: true
        }
      }),
    addFailure: state =>
      Object.assign({}, state, {
        add: {
          pending: false,
          failed: true,
          success: false
        }
      })
  },
  effects: {
    async getAll() {
      this.getAllPending();
      try {
        this.getAllSuccess(await cakesService.getAll());
      } catch (e) {
        this.getAllFailure();
      }
    },
    async add(cake) {
      this.addPending();
      try {
        this.addSuccess(await cakesService.add(cake));
      } catch (e) {
        this.addFailure();
      }
    }
  }
};

export default cakes;
