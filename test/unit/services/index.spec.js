import test from 'ava';
import { default as cakeService } from '../../../src/services/cakes';
const testee = require('../../../src/services');

test('exposes cake service', t => {
  t.is(testee.cakes, cakeService);
});
