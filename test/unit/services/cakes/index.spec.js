import test from 'ava';
import td from 'testdouble';

const BASE_URL =
  'http://ec2-52-209-201-89.eu-west-1.compute.amazonaws.com:5000/api/cakes';

const setup = () => {
  const stubs = {
    axios: td.replace('axios')
  };
  const testee = require('../../../../src/services/cakes').default;
  return { stubs, testee };
};

test.beforeEach(t => {
  t.context = setup();
});

test.afterEach.always(t => td.reset());

test('getAll - calls correct endpoint', async t => {
  const { testee, stubs } = t.context;
  try {
    await testee.getAll();
  } catch (e) {}
  td.verify(stubs.axios.get(BASE_URL));
  t.pass();
});

test('getAll - returns data from payload', async t => {
  const { testee, stubs } = t.context;
  const response = {
    data: 'the data'
  };
  td.when(stubs.axios.get(BASE_URL)).thenResolve(response);
  t.is(await testee.getAll(), response.data);
});

test('getAll - propagates error', async t => {
  const { testee, stubs } = t.context;
  const error = new Error('Oh No');
  td.when(stubs.axios.get(BASE_URL)).thenReject(error);

  try {
    await testee.getAll();
    t.fail('expected error');
  } catch (e) {
    t.is(e, error);
  }
});

test('add - calls correct endpoint', async t => {
  const cake = {
    a: 'cake'
  };
  const { testee, stubs } = t.context;
  try {
    await testee.add(cake);
  } catch (e) {}
  td.verify(stubs.axios.post(BASE_URL, cake));
  t.pass();
});

test('add - returns data from payload', async t => {
  const cake = {
    a: 'cake'
  };
  const { testee, stubs } = t.context;
  const response = { data: 'stuff' };
  td.when(stubs.axios.post(BASE_URL, cake)).thenResolve(response);
  t.is(await testee.add(cake), response.data);
});

test('add - propagates error', async t => {
  const cake = {
    a: 'cake'
  };
  const { testee, stubs } = t.context;
  const error = new Error('Oh No');
  td.when(stubs.axios.post(BASE_URL, cake)).thenReject(error);

  try {
    await testee.add(cake);
    t.fail('expected error');
  } catch (e) {
    t.is(e, error);
  }
});
