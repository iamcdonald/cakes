import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import td from 'testdouble';
import { AddCakeForm, ErrorBox } from '../../../../src/components';

const generateConnectStub = () => {
  const stub = {
    connect: td.function()
  };
  const addComp = td.function();
  stub._addComp = addComp;
  td
    .when(stub.connect(td.matchers.isA(Function), td.matchers.isA(Function)))
    .thenReturn(addComp);
  td.when(addComp(td.matchers.isA(Function))).thenReturn('CONNECTED-COMPONENT');
  return stub;
};

const setup = () => {
  const stubs = {
    'react-redux': td.replace('react-redux', generateConnectStub())
  };
  const testee = require('../../../../src/containers/AddCakeForm');
  return { stubs, testee };
};

test.beforeEach(t => {
  t.context = setup();
});

test.afterEach.always(t => td.reset());

const createComp = (Comp, props) => ({
  comp: shallow(<Comp {...props} />)
});

test('mapState - maps cakes state', t => {
  const { testee } = t.context;
  const state = {
    cakes: {
      add: {
        pending: false,
        failed: false,
        success: false
      }
    }
  };
  t.deepEqual(testee.mapState(state), {
    failed: false,
    success: false
  });
});

test('mapDispatch - exposes getAll action', t => {
  const { testee } = t.context;
  const dispatch = {
    cakes: {
      add: td.function()
    }
  };
  t.deepEqual(testee.mapDispatch(dispatch), {
    addCake: dispatch.cakes.add
  });
});

test('AddCakeFormContainer - render - renders a AddCakeForm', t => {
  const { testee } = t.context;
  const props = {
    addCake: td.function(),
    failed: false,
    success: false,
    history: {}
  };
  const { comp } = createComp(testee.AddCakeFormContainer, props);
  const addCakeForm = comp.find(AddCakeForm);
  t.truthy(addCakeForm.exists());
});

test('AddCakeFormContainer - render - when submitted it calls props.addCake', t => {
  const { testee } = t.context;
  const props = {
    addCake: td.function(),
    failed: false,
    success: false,
    history: {}
  };
  const { comp } = createComp(testee.AddCakeFormContainer, props);
  const addCakeForm = comp.find(AddCakeForm);
  const cake = 'cake';
  addCakeForm.simulate('submit', cake);
  td.verify(props.addCake(cake));
  t.pass();
});

test('AddCakeFormContainer - render - when cancelled it navigates to cakes page', t => {
  const { testee } = t.context;
  const props = {
    addCake: td.function(),
    history: {
      push: td.function()
    },
    failed: false,
    success: false
  };
  const { comp } = createComp(testee.AddCakeFormContainer, props);
  const addCakeForm = comp.find(AddCakeForm);
  const cake = 'cake';
  addCakeForm.simulate('cancel', cake);
  td.verify(props.history.push('/'));
  t.pass();
});

test('AddCakeFormContainer - render - when failed it shows error box', t => {
  const { testee } = t.context;
  const props = {
    addCake: td.function(),
    history: {
      push: td.function()
    },
    failed: true,
    success: false
  };
  const { comp } = createComp(testee.AddCakeFormContainer, props);
  const errorBox = comp.find(ErrorBox);
  t.truthy(errorBox.contains('Failed to add cake, please try again.'));
});

test('AddCakeFormContainer - componentDidUpdate - when success - navigates to cakes page', t => {
  const { testee } = t.context;
  const props = {
    addCake: td.function(),
    history: {
      push: td.function()
    },
    failed: true,
    success: false
  };
  const comp = new testee.AddCakeFormContainer(props);
  comp.componentWillUpdate({ success: true });
  td.verify(props.history.push('/'));
  t.pass();
});

test('AddCakeFormContainer - componentDidUpdate - when no success - does not navigate to cakes page', t => {
  const { testee } = t.context;
  const props = {
    addCake: td.function(),
    history: {
      push: td.function()
    },
    failed: true,
    success: false
  };
  const comp = new testee.AddCakeFormContainer(props);
  comp.componentWillUpdate({ success: false });
  td.verify(props.history.push('/'), { times: 0 });
  t.pass();
});

test('connect - AddCakeFormContainer should be correctly connected to store', t => {
  const { testee, stubs } = t.context;
  t.is(testee.default, 'CONNECTED-COMPONENT');
  const compCaptor = td.matchers.captor();
  td.verify(stubs['react-redux']._addComp(compCaptor.capture()));
  t.is(compCaptor.values[0], testee.AddCakeFormContainer);
});
