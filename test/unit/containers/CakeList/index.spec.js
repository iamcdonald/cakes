import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import td from 'testdouble';
import { CakeList, Loader, ErrorBox } from '../../../../src/components';

const generateConnectStub = () => {
  const stub = {
    connect: td.function()
  };
  const addComp = td.function();
  stub._addComp = addComp;
  td
    .when(stub.connect(td.matchers.isA(Function), td.matchers.isA(Function)))
    .thenReturn(addComp);
  td.when(addComp(td.matchers.isA(Function))).thenReturn('CONNECTED-COMPONENT');
  return stub;
};

const setup = () => {
  const stubs = {
    'react-redux': td.replace('react-redux', generateConnectStub())
  };
  const testee = require('../../../../src/containers/CakeList');
  return { stubs, testee };
};

test.beforeEach(t => {
  t.context = setup();
});

test.afterEach.always(t => td.reset());

const createComp = (Comp, props) => ({
  comp: shallow(<Comp {...props} />)
});

test('mapState - maps cakes state', t => {
  const { testee } = t.context;
  const state = {
    cakes: {
      list: 'all-cakes',
      getAll: {
        pending: false,
        failed: false
      }
    }
  };
  t.deepEqual(testee.mapState(state), {
    cakes: state.cakes.list,
    pending: false,
    failed: false
  });
});

test('mapDispatch - exposes getAll action', t => {
  const { testee } = t.context;
  const dispatch = {
    cakes: {
      getAll: td.function()
    }
  };
  t.deepEqual(testee.mapDispatch(dispatch), {
    getCakes: dispatch.cakes.getAll
  });
});

test('CakeListContainer - componentDidMount - when no cakes - raises action to get cakes', t => {
  const { testee } = t.context;
  const props = {
    getCakes: td.function(),
    cakes: null
  };
  const comp = new testee.CakeListContainer(props);
  comp.componentDidMount();
  td.verify(props.getCakes());
  t.pass();
});

test('CakeListContainer - componentDidMount - when cakes - does not raises action to get cakes', t => {
  const { testee } = t.context;
  const props = {
    getCakes: td.function(),
    cakes: []
  };
  const comp = new testee.CakeListContainer(props);
  comp.componentDidMount();
  td.verify(props.getCakes(), { times: 0 });
  t.pass();
});

test('CakeListContainer - render - when call pending - renders a Loader', t => {
  const { testee } = t.context;
  const props = {
    getCakes: td.function(),
    cakes: [],
    pending: true,
    failed: true
  };
  const { comp } = createComp(testee.CakeListContainer, props);
  t.truthy(comp.find(Loader).exists);
});

test('CakeListContainer - render - when not pending and failed - renders a ErrorBox with correct message', t => {
  const { testee } = t.context;
  const props = {
    getCakes: td.function(),
    cakes: [],
    pending: false,
    failed: true
  };
  const { comp } = createComp(testee.CakeListContainer, props);
  const errorBox = comp.find(ErrorBox);
  t.is(errorBox.props().children, 'An error has occurred fetching the cakes.');
});

test('CakeListContainer - render - when not pending or failed and has cakes  - renders a CakeListComponent with cakes from props', t => {
  const { testee } = t.context;
  const props = {
    getCakes: td.function(),
    cakes: [],
    pending: false,
    failed: false
  };
  const { comp } = createComp(testee.CakeListContainer, props);
  const cakeListComponent = comp.find(CakeList);
  t.is(cakeListComponent.props().cakes, props.cakes);
});

test('CakeListContainer - render - when not pending or failed and has no cakes  - renders nothing', t => {
  const { testee } = t.context;
  const props = {
    getCakes: td.function(),
    cakes: null,
    pending: false,
    failed: false
  };
  const { comp } = createComp(testee.CakeListContainer, props);
  t.falsy(comp.props().children);
});

test('connect - CakeListContainer should be correctly connected to store', t => {
  const { testee, stubs } = t.context;
  t.is(testee.default, 'CONNECTED-COMPONENT');
  const compCaptor = td.matchers.captor();
  td.verify(stubs['react-redux']._addComp(compCaptor.capture()));
  t.is(compCaptor.values[0], testee.CakeListContainer);
});
