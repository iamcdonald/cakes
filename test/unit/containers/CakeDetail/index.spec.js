import test from 'ava';
import td from 'testdouble';
import { CakeDetail } from '../../../../src/components';

const generateConnectStub = () => {
  const stub = {
    connect: td.function()
  };
  const addComp = td.function();
  stub._addComp = addComp;
  td.when(stub.connect(td.matchers.isA(Function))).thenReturn(addComp);
  td.when(addComp(td.matchers.isA(Function))).thenReturn('CONNECTED-COMPONENT');
  return stub;
};

const setup = () => {
  const stubs = {
    'react-redux': td.replace('react-redux', generateConnectStub())
  };
  const testee = require('../../../../src/containers/CakeDetail');
  return { stubs, testee };
};

test.beforeEach(t => {
  t.context = setup();
});

test.afterEach.always(t => td.reset());

test('mapState - maps chosen cake state', t => {
  const { testee } = t.context;
  const state = {
    cakes: {
      list: [
        {
          id: '1'
        },
        {
          id: '2'
        },
        {
          id: '3'
        }
      ]
    }
  };
  const props = {
    cakeId: '2'
  };
  t.deepEqual(testee.mapState(state, props).cake, state.cakes.list[1]);
});

test('connect - AddCakeFormContainer should be correctly connected to store', t => {
  const { testee, stubs } = t.context;
  t.is(testee.default, 'CONNECTED-COMPONENT');
  const compCaptor = td.matchers.captor();
  td.verify(stubs['react-redux']._addComp(compCaptor.capture()));
  t.is(compCaptor.values[0], CakeDetail);
});
