import test from 'ava';
import { default as CakeList } from '../../../src/containers/CakeList';
import { default as AddCakeForm } from '../../../src/containers/AddCakeForm';
import { default as CakeDetail } from '../../../src/containers/CakeDetail';
const testee = require('../../../src/containers');

test('exposes CakeList container', t => {
  t.is(testee.CakeList, CakeList);
});

test('exposes AddCakeForm container', t => {
  t.is(testee.AddCakeForm, AddCakeForm);
});

test('exposes CakeDetail container', t => {
  t.is(testee.CakeDetail, CakeDetail);
});
