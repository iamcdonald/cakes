import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import ErrorBox from '../../../../src/components/ErrorBox';

const createComp = message => ({
  comp: shallow(<ErrorBox>{message}</ErrorBox>)
});

test('it renders message', t => {
  const message = 'a message';
  const { comp } = createComp(message);
  t.truthy(comp.contains(message));
});
