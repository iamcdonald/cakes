import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import TextInput from '../../../../../src/components/AddCakeForm/TextInput';

const createComp = props => ({
  comp: shallow(<TextInput {...props} />)
});

test('it renders an input', t => {
  const props = {
    name: 'Name',
    text: 'Text',
    onChange: () => {},
    type: 'text'
  };
  const { comp } = createComp(props);
  const input = comp.find('input');
  t.is(input.props().name, props.name);
  t.is(input.props().onChange, props.onChange);
  t.is(input.props().type, props.type);
});

test('it renders a label for the input', t => {
  const props = { name: 'Name', text: 'Text', onChange: () => {} };
  const { comp } = createComp(props);
  const label = comp.find('label');
  t.is(label.props().htmlFor, props.name);
  t.is(label.find('span').text(), `${props.text}:`);
});
