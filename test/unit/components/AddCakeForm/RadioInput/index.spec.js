import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import RadioInput from '../../../../../src/components/AddCakeForm/RadioInput';

const createComp = props => ({
  comp: shallow(<RadioInput {...props} />)
});

test('it renders a label for entire radio input', t => {
  const props = {
    name: 'Name',
    text: 'Text',
    options: [
      {
        value: 12
      },
      {
        value: 24
      }
    ],
    onChange: () => {}
  };
  const { comp } = createComp(props);
  const label = comp
    .find('label')
    .findWhere(e => e.props().htmlFor === props.name);
  t.is(label.props().htmlFor, props.name);
  t.is(
    label
      .find('span')
      .at(0)
      .text(),
    `${props.text}:`
  );
});

test('it renders an radio input for each option', t => {
  const props = {
    name: 'Name',
    text: 'Text',
    options: [
      {
        value: 12
      },
      {
        value: 24
      }
    ],
    onChange: () => {}
  };
  const { comp } = createComp(props);
  comp.find('input').forEach((input, idx) => {
    t.is(input.props().name, props.name);
    t.is(input.props().type, 'radio');
    t.is(input.props().value, props.options[idx].value);
    t.is(input.props().onChange, props.onChange);
  });
});

test('it renders a label for each input', t => {
  const props = {
    name: 'Name',
    text: 'Text',
    options: [
      {
        value: 12
      },
      {
        value: 24
      }
    ],
    onChange: () => {}
  };
  const { comp } = createComp(props);
  comp.find('input').forEach((input, idx) => {
    const label = comp
      .find('label')
      .findWhere(e => e.props().htmlFor === input.props().value);
    t.is(label.props().htmlFor, input.props().value);
    t.is(label.find('span').text(), `${input.props().value}`);
  });
});
