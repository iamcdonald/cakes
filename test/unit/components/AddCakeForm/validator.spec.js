import test from 'ava';
import { validateCake } from '../../../../src/components/AddCakeForm/validator';

test('when cake has all values - returns true', t => {
  const cake = {
    name: 'name',
    comment: 'comment',
    imageUrl: 'imageUrl',
    yumFactor: 4
  };
  t.is(validateCake(cake), true);
});

test('when cake is missing name - returns false', t => {
  const cake = {
    comment: 'comment',
    imageUrl: 'imageUrl',
    yumFactor: 4
  };
  t.is(validateCake(cake), false);
});

test('when cake is missing comment - returns false', t => {
  const cake = {
    name: 'name',
    imageUrl: 'imageUrl',
    yumFactor: 4
  };
  t.is(validateCake(cake), false);
});

test('when cake is missing imageUrl - returns false', t => {
  const cake = {
    name: 'name',
    comment: 'comment',
    yumFactor: 4
  };
  t.is(validateCake(cake), false);
});

test('when cake is missing yumFactor - returns false', t => {
  const cake = {
    name: 'name',
    comment: 'comment',
    imageUrl: 'imageUrl'
  };
  t.is(validateCake(cake), false);
});
