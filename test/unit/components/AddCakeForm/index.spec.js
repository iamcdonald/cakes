import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import td from 'testdouble';
import TextInput from '../../../../src/components/AddCakeForm/TextInput';
import RadioInput from '../../../../src/components/AddCakeForm/RadioInput';

const setup = () => {
  const stubs = {
    validator: td.replace('../../../../src/components/AddCakeForm/validator', {
      validateCake: td.function()
    })
  };
  const testee = require('../../../../src/components/AddCakeForm');
  return { testee, stubs };
};

const DEFAULT_PROPS = {
  onCancel: () => {},
  onSubmit: () => {}
};

const createComp = (Comp, props = DEFAULT_PROPS) => ({
  comp: shallow(<Comp {...props} />)
});

test.beforeEach(t => {
  t.context = setup();
});

test.afterEach(t => td.reset());

test('it renders a text input for name', t => {
  const { testee } = t.context;
  const { comp } = createComp(testee.default);
  const nameInput = comp
    .find(TextInput)
    .findWhere(e => e.props().name === 'name');
  t.truthy(nameInput.exists());
  t.is(nameInput.props().name, 'name');
  t.is(nameInput.props().text, 'Name');
  t.is(nameInput.props().type, 'text');
});

test('when name input is changed it is reflected in state', t => {
  const { testee } = t.context;
  const { comp } = createComp(testee.default);
  const nameInput = comp
    .find(TextInput)
    .findWhere(e => e.props().name === 'name');
  const event = { currentTarget: { value: 'a name' } };
  nameInput.simulate('change', event);
  t.is(comp.state().cake.name, event.currentTarget.value);
});

test('it renders a text input for comment', t => {
  const { testee } = t.context;
  const { comp } = createComp(testee.default);
  const commentInput = comp
    .find(TextInput)
    .findWhere(e => e.props().name === 'comment');
  t.truthy(commentInput.exists());
  t.is(commentInput.props().name, 'comment');
  t.is(commentInput.props().text, 'Comment');
  t.is(commentInput.props().type, 'text');
});

test('when comment input is changed it is reflected in state', t => {
  const { testee } = t.context;
  const { comp } = createComp(testee.default);
  const commentInput = comp
    .find(TextInput)
    .findWhere(e => e.props().name === 'comment');
  const event = { currentTarget: { value: 'a comment' } };
  commentInput.simulate('change', event);
  t.is(comp.state().cake.comment, event.currentTarget.value);
});

test('it renders a text input for image url', t => {
  const { testee } = t.context;
  const { comp } = createComp(testee.default);
  const imageURLInput = comp
    .find(TextInput)
    .findWhere(e => e.props().name === 'imageUrl');
  t.truthy(imageURLInput.exists());
  t.is(imageURLInput.props().name, 'imageUrl');
  t.is(imageURLInput.props().text, 'Image URL');
  t.is(imageURLInput.props().type, 'url');
});

test('when imageURL input is changed it is reflected in state', t => {
  const { testee } = t.context;
  const { comp } = createComp(testee.default);
  const imageURLInput = comp
    .find(TextInput)
    .findWhere(e => e.props().name === 'imageUrl');
  const event = { currentTarget: { value: 'http://www.a-url.com/image.png' } };
  imageURLInput.simulate('change', event);
  t.is(comp.state().cake.imageUrl, event.currentTarget.value);
});

test('it renders a radio input for yum factor', t => {
  const { testee } = t.context;
  const { comp } = createComp(testee.default);
  const radioInput = comp.find(RadioInput);
  t.truthy(radioInput.exists());
  t.is(radioInput.props().name, 'yums');
  t.is(radioInput.props().text, 'Yumz?');
  t.is(radioInput.props().options, testee.YUM_OPTIONS);
});

test('when yum factor input is changed it is reflected in state', t => {
  const { testee } = t.context;
  const { comp } = createComp(testee.default);
  const radioInput = comp.find(RadioInput);
  const event = { currentTarget: { value: 5 } };
  radioInput.simulate('change', event);
  t.is(comp.state().cake.yumFactor, event.currentTarget.value);
});

test('it renders a cancel button', t => {
  const { testee } = t.context;
  const { comp } = createComp(testee.default);
  const cancelButton = comp
    .find('button')
    .findWhere(e => e.props().type === 'button');
  t.truthy(cancelButton.exists());
  t.is(cancelButton.props().type, 'button');
  t.is(cancelButton.text(), 'Cancel');
});

test('when cancel button clicked it calls onCancel prop', t => {
  const { testee } = t.context;
  const props = {
    onCancel: td.function(),
    onSubmit: td.function()
  };
  const { comp } = createComp(testee.default, props);
  const cancelButton = comp
    .find('button')
    .findWhere(e => e.props().type === 'button');
  cancelButton.simulate('click');
  td.verify(props.onCancel());
  t.pass();
});

test('it renders a submit button', t => {
  const { testee } = t.context;
  const { comp } = createComp(testee.default);
  const submitButton = comp
    .find('button')
    .findWhere(e => e.props().type === 'submit');
  t.truthy(submitButton.exists());
  t.is(submitButton.props().type, 'submit');
  t.is(submitButton.text(), 'Submit');
});

test('when form submitted -  prevents default action', t => {
  const { testee } = t.context;
  const props = {
    onCancel: td.function(),
    onSubmit: td.function()
  };
  const { comp } = createComp(testee.default, props);
  const state = {};
  comp.setState(state);
  const form = comp.find('form');
  const event = { preventDefault: td.function() };
  form.simulate('submit', event);
  td.verify(event.preventDefault());
  t.pass();
});

test('when form submitted - and valid - it calls onSubmit with cake', t => {
  const { testee, stubs } = t.context;
  const props = {
    onCancel: td.function(),
    onSubmit: td.function()
  };
  const { comp } = createComp(testee.default, props);
  const state = {
    cake: {
      name: 'name',
      imageUrl: 'url',
      comment: 'comment',
      yumFactor: 5
    },
    yeah: true
  };
  td.when(stubs.validator.validateCake(state.cake)).thenReturn(true);
  comp.setState(state);
  const form = comp.find('form');
  const event = { preventDefault: td.function() };
  form.simulate('submit', event);
  td.verify(props.onSubmit(state.cake));
  t.pass();
});

test('when form submitted - and invalid - it does not call onSubmit', t => {
  const { testee, stubs } = t.context;
  const props = {
    onCancel: td.function(),
    onSubmit: td.function()
  };
  const { comp } = createComp(testee.default, props);
  const state = {
    cake: {
      name: 'name',
      imageUrl: 'url',
      comment: 'comment',
      yumFactor: 5
    },
    yeah: true
  };
  td.when(stubs.validator.validateCake(state.cake)).thenReturn(false);
  comp.setState(state);
  const form = comp.find('form');
  const event = { preventDefault: td.function() };
  form.simulate('submit', event);
  td.verify(props.onSubmit(state.cake), { times: 0 });
  t.pass();
});

test('when form submitted - and invalid - it sets invalid state', t => {
  const { testee, stubs } = t.context;
  const props = {
    onCancel: td.function(),
    onSubmit: td.function()
  };
  const { comp } = createComp(testee.default, props);
  const state = {
    cake: {
      name: 'name',
      imageUrl: 'url',
      comment: 'comment',
      yumFactor: 5
    },
    invalid: false
  };
  td.when(stubs.validator.validateCake(state.cake)).thenReturn(false);
  comp.setState(state);
  const form = comp.find('form');
  const event = { preventDefault: td.function() };
  form.simulate('submit', event);
  t.is(comp.state().invalid, true);
});
