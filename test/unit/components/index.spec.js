import test from 'ava';
import { default as AddCakeForm } from '../../../src/components/AddCakeForm';
import { default as CakeDetail } from '../../../src/components/CakeDetail';
import { default as CakeList } from '../../../src/components/CakeList';
import { default as ErrorBox } from '../../../src/components/ErrorBox';
import { default as Header } from '../../../src/components/Header';
import { default as Loader } from '../../../src/components/Loader';
const testee = require('../../../src/components/index');

test('exposes AddCakeForm component', t => {
  t.is(testee.AddCakeForm, AddCakeForm);
});

test('exposes CakeDetail component', t => {
  t.is(testee.CakeDetail, CakeDetail);
});

test('exposes CakeList component', t => {
  t.is(testee.CakeList, CakeList);
});

test('exposes ErrorBox component', t => {
  t.is(testee.ErrorBox, ErrorBox);
});

test('exposes Header component', t => {
  t.is(testee.Header, Header);
});

test('exposes Loader component', t => {
  t.is(testee.Loader, Loader);
});
