import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import Header from '../../../../src/components/Header';
import { Link } from 'react-router-dom';

const createComp = props => ({
  comp: shallow(<Header {...props} />)
});

test('it renders a title', t => {
  const props = {
    title: 'title',
    link: {
      path: 'path',
      text: 'text'
    }
  };
  const { comp } = createComp(props);
  t.is(comp.find('h1').text(), props.title);
});

test('when link prop provided - it renders a link', t => {
  const props = {
    title: 'title',
    link: {
      path: 'path',
      text: 'text'
    }
  };
  const { comp } = createComp(props);
  const link = comp.find(Link);
  t.truthy(link.contains(props.link.text));
  t.is(link.props().to, props.link.path);
});

test('when link prop not provided - it does not render a link', t => {
  const props = {
    title: 'title'
  };
  const { comp } = createComp(props);
  t.falsy(comp.find(Link).exists());
});
