import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import Loader from '../../../../src/components/Loader';

const createComp = message => ({
  comp: shallow(<Loader />)
});

test('it renders loader', t => {
  const { comp } = createComp();
  t.truthy(comp.contains('Loading...'));
});
