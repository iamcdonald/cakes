import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import CakeDetail from '../../../../src/components/CakeDetail';

const createComp = props => ({
  comp: shallow(<CakeDetail {...props} />)
});

test('it renders image', t => {
  const cake = {
    imageUrl: 'a-url'
  };
  const { comp } = createComp({ cake });
  const image = comp.find('img');
  t.is(image.props().src, cake.imageUrl);
});

test('it renders cake name', t => {
  const cake = {
    name: 'name'
  };
  const { comp } = createComp({ cake });
  const nameDef = comp
    .find('dl')
    .find('div')
    .at(0);
  t.is(nameDef.find('dt').text(), 'Name');
  t.is(nameDef.find('dd').text(), cake.name);
});

test('it renders comment name', t => {
  const cake = {
    comment: 'name'
  };
  const { comp } = createComp({ cake });
  const commentDef = comp
    .find('dl')
    .find('div')
    .at(1);
  t.is(commentDef.find('dt').text(), 'Comment');
  t.is(commentDef.find('dd').text(), cake.comment);
});

test('it renders yum factor name', t => {
  const cake = {
    yumFactor: 'name'
  };
  const { comp } = createComp({ cake });
  const yumDef = comp
    .find('dl')
    .find('div')
    .at(2);
  t.is(yumDef.find('dt').text(), 'Yums');
  t.is(yumDef.find('dd').text(), cake.yumFactor);
});
