import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import { Link } from 'react-router-dom';
import CakeItem from '../../../../../src/components/CakeList/CakeItem';

const createComp = props => ({
  comp: shallow(<CakeItem {...props} />)
});

test('it renders link with correct path', t => {
  const cake = {
    imageUrl: 'http://www.cakes.plumbers/cake.png',
    name: 'cake'
  };
  const { comp } = createComp({ cake });
  const link = comp.find(Link);
  t.is(link.props().to, `/detail/${cake.id}`);
});

test('within link - it renders image with correct src', t => {
  const cake = {
    imageUrl: 'http://www.cakes.plumbers/cake.png',
    name: 'cake'
  };
  const { comp } = createComp({ cake });
  const image = comp.find(Link).find('img');
  t.is(image.props().src, cake.imageUrl);
});

test('within link - it renders name', t => {
  const cake = {
    imageUrl: 'http://www.cakes.plumbers/cake.png',
    name: 'cake'
  };
  const { comp } = createComp({ cake });
  t.is(
    comp
      .find(Link)
      .find('span')
      .text(),
    cake.name
  );
});
