import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import CakeList from '../../../../src/components/CakeList';

const createComp = props => ({
  comp: shallow(<CakeList {...props} />)
});

test('it renders a ul', t => {
  const cakes = [];
  const { comp } = createComp({ cakes });
  t.truthy(comp.find('ul').exists());
});

test('it populates the ul with a CakeItem for each cake', t => {
  const cakes = [
    { imageURL: 'url-1', title: 'cake-1', id: '1' },
    { imageURL: 'url-2', title: 'cake-2', id: '2' },
    { imageURL: 'url-3', title: 'cake-3', id: '3' }
  ];
  const { comp } = createComp({ cakes });
  const cakeItems = comp.find('ul > CakeItem');
  t.is(cakeItems.length, cakes.length);
  cakeItems.forEach((cakeItem, idx) => {
    t.is(cakeItem.props().cake, cakes[idx]);
  });
});
