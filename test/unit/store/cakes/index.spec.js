import test from 'ava';
import td from 'testdouble';

const setup = () => {
  const stubs = {
    services: td.replace('../../../../src/services', {
      cakes: {
        getAll: td.function(),
        add: td.function()
      }
    })
  };
  const testee = require('../../../../src/store/cakes');
  return { testee, stubs };
};

test.beforeEach(t => {
  t.context = setup();
});

test.afterEach.always(t => td.reset());

test('state - default', t => {
  const { testee } = t.context;
  t.deepEqual(testee.default.state, {
    list: null,
    getAll: {
      pending: false,
      failed: false
    },
    add: {
      pending: false,
      failed: false,
      success: false
    }
  });
});

test('reducers - getAllPending - sets pending state', t => {
  const { testee } = t.context;
  const state = {
    list: [1, 2, 3, 4],
    getAll: {
      pending: false,
      failed: true
    },
    add: {
      pending: true,
      failed: true,
      success: true
    }
  };
  t.deepEqual(testee.default.reducers.getAllPending(state), {
    list: null,
    getAll: {
      pending: true,
      failed: false
    },
    add: {
      pending: true,
      failed: true,
      success: true
    }
  });
});

test('reducers - getAllSuccess - adds cakes to store', t => {
  const { testee } = t.context;
  const state = {
    list: [],
    getAll: {
      pending: true,
      failed: true
    },
    add: {
      pending: true,
      failed: true,
      success: true
    }
  };
  const cakes = [1, 2, 3, 4];
  t.deepEqual(testee.default.reducers.getAllSuccess(state, cakes), {
    list: cakes,
    getAll: {
      pending: false,
      failed: false
    },
    add: {
      pending: true,
      failed: true,
      success: true
    }
  });
});

test('reducers - getAllFailure - sets failed state', t => {
  const { testee } = t.context;
  const state = {
    list: [1, 2, 3, 4],
    getAll: {
      pending: true,
      failed: false
    },
    add: {
      pending: true,
      failed: true,
      success: true
    }
  };
  t.deepEqual(testee.default.reducers.getAllFailure(state), {
    list: null,
    getAll: {
      pending: false,
      failed: true
    },
    add: {
      pending: true,
      failed: true,
      success: true
    }
  });
});

test('effects - getAll - sets pending state', async t => {
  const { testee } = t.context;
  const self = {
    getAllPending: td.function(),
    getAllSuccess: td.function(),
    getAllFailure: td.function()
  };
  await testee.default.effects.getAll.call(self);
  td.verify(self.getAllPending());
  t.pass();
});

test('effects - getAll - when call successful - sets success state', async t => {
  const { testee, stubs } = t.context;
  const self = {
    getAllPending: td.function(),
    getAllSuccess: td.function(),
    getAllFailure: td.function()
  };
  const data = [1, 2, 3, 4];
  td.when(stubs.services.cakes.getAll()).thenResolve(data);
  await testee.default.effects.getAll.call(self);
  td.verify(self.getAllSuccess(data));
  t.pass();
});

test('effects - getAll - when call unsuccessful - sets failure state', async t => {
  const { testee, stubs } = t.context;
  const self = {
    getAllPending: td.function(),
    getAllSuccess: td.function(),
    getAllFailure: td.function()
  };
  td.when(stubs.services.cakes.getAll()).thenReject({});
  await testee.default.effects.getAll.call(self);
  td.verify(self.getAllFailure());
  t.pass();
});

test('reducers - addPending - sets pending state', t => {
  const { testee } = t.context;
  const state = {
    list: [1, 2, 3, 4],
    getAll: {
      pending: true,
      failed: true
    },
    add: {
      pending: false,
      failed: true,
      success: true
    }
  };
  t.deepEqual(testee.default.reducers.addPending(state), {
    list: [1, 2, 3, 4],
    getAll: {
      pending: true,
      failed: true
    },
    add: {
      pending: true,
      failed: false,
      success: false
    }
  });
});

test('reducers - addSuccess - adds cakes to store', t => {
  const { testee } = t.context;
  const state = {
    list: [1, 2, 3, 4],
    getAll: {
      pending: true,
      failed: true
    },
    add: {
      pending: true,
      failed: true,
      success: false
    }
  };
  const cake = 5;
  t.deepEqual(testee.default.reducers.addSuccess(state, cake), {
    list: [5, 1, 2, 3, 4],
    getAll: {
      pending: true,
      failed: true
    },
    add: {
      pending: false,
      failed: false,
      success: true
    }
  });
});

test('reducers - addFailure - sets failed state', t => {
  const { testee } = t.context;
  const state = {
    list: [1, 2, 3, 4],
    getAll: {
      pending: true,
      failed: true
    },
    add: {
      pending: true,
      failed: false,
      success: true
    }
  };
  t.deepEqual(testee.default.reducers.addFailure(state), {
    list: [1, 2, 3, 4],
    getAll: {
      pending: true,
      failed: true
    },
    add: {
      pending: false,
      failed: true,
      success: false
    }
  });
});

test('effects - add - sets pending state', async t => {
  const { testee } = t.context;
  const self = {
    addPending: td.function(),
    addSuccess: td.function(),
    addFailure: td.function()
  };
  const cake = 'cake';
  await testee.default.effects.add.call(self, cake);
  td.verify(self.addPending());
  t.pass();
});

test('effects - add - when call successful - sets success state', async t => {
  const { testee, stubs } = t.context;
  const self = {
    addPending: td.function(),
    addSuccess: td.function(),
    addFailure: td.function()
  };
  const cake = 'cake';
  const newCake = 'new-cake';
  td.when(stubs.services.cakes.add(cake)).thenResolve(newCake);
  await testee.default.effects.add.call(self, cake);
  td.verify(self.addSuccess(newCake));
  t.pass();
});

test('effects - add - when call unsuccessful - sets failure state', async t => {
  const { testee, stubs } = t.context;
  const self = {
    addPending: td.function(),
    addSuccess: td.function(),
    addFailure: td.function()
  };
  const cake = 'cake';
  td.when(stubs.services.cakes.add(cake)).thenReject({});
  await testee.default.effects.add.call(self, cake);
  td.verify(self.addFailure());
  t.pass();
});
