import test from 'ava';
import td from 'testdouble';

const setup = () => {
  const stubs = {
    rematch: td.replace('@rematch/core'),
    cakesStore: td.replace('../../../src/store/cakes', 'CAKE_STORE')
  };
  td
    .when(
      stubs.rematch.init({
        models: {
          cakes: 'CAKE_STORE'
        }
      })
    )
    .thenReturn('the initialized store');
  const testee = require('../../../src/store/index');
  return { testee, stubs };
};

test.beforeEach(t => {
  t.context = setup();
});

test.afterEach.always(t => td.reset());

test('returns initialized store', t => {
  const { testee } = t.context;
  t.is(testee.default, 'the initialized store');
});
