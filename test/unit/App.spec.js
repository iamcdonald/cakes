import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import App from '../../src/App';
import { HashRouter as Router, Route } from 'react-router-dom';
import { Cakes, AddCake } from '../../src/pages';

const createComp = () => shallow(<App />);

test('renders Router', t => {
  const app = createComp();
  t.truthy(app.find(Router).exists());
});

test('Router has route for main cakes page', t => {
  const app = createComp();
  const route = app
    .find(Router)
    .find(Route)
    .at(0);
  t.is(route.props().exact, true);
  t.is(route.props().path, '/');
  t.is(route.props().component, Cakes);
});

test('Router has route for add cake page', t => {
  const app = createComp();
  const route = app
    .find(Router)
    .find(Route)
    .at(1);
  t.is(route.props().path, '/add');
  t.is(route.props().component, AddCake);
});
