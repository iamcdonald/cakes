import React from 'react';
import App from '../../src/App';
import test from 'ava';
import td from 'testdouble';
import { Provider } from 'react-redux';
import store from '../../src/store';

const setup = () => {
  const stubs = {
    'react-dom': td.replace('react-dom', {
      render: td.function()
    })
  };
  global.document = {
    getElementById: x => x
  };
  require('../../src/index');
  return { stubs };
};

test.beforeEach(t => {
  t.context = setup();
});

test.afterEach.always(t => td.reset());

test('renders application', t => {
  const { stubs } = t.context;
  td.verify(
    stubs['react-dom'].render(
      <Provider store={store}>
        <App />
      </Provider>,
      'app'
    )
  );
  t.pass();
});
