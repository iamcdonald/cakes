import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import Cake from '../../../../src/pages/Cake';
import { Header } from '../../../../src/components';
import { CakeDetail } from '../../../../src/containers';

const createComp = props => ({
  comp: shallow(<Cake {...props} />)
});

test('it renders the header', t => {
  const props = {
    match: {
      params: {
        id: '2'
      }
    }
  };
  const { comp } = createComp(props);
  const header = comp.find(Header);
  t.is(header.props().title, 'Cake Detail');
  t.deepEqual(header.props().link, { path: '/', text: 'view all cakes' });
});

test('it renders cake detail view with id', t => {
  const props = {
    match: {
      params: {
        id: '2'
      }
    }
  };
  const { comp } = createComp(props);
  const cakeDetail = comp.find(CakeDetail);
  t.is(cakeDetail.props().cakeId, props.match.params.id);
});
