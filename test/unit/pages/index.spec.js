import test from 'ava';
import { default as CakesPage } from '../../../src/pages/Cakes';
import { default as AddCakePage } from '../../../src/pages/AddCake';
import { default as CakePage } from '../../../src/pages/Cake';
const testee = require('../../../src/pages');

test('exposes Cakes page', t => {
  t.is(testee.Cakes, CakesPage);
});

test('exposes AddCake page', t => {
  t.is(testee.AddCake, AddCakePage);
});

test('exposes Cake page', t => {
  t.is(testee.Cake, CakePage);
});
