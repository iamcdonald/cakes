import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import AddCake from '../../../../src/pages/AddCake';
import { Header } from '../../../../src/components';
import { AddCakeForm } from '../../../../src/containers';

const createComp = props => ({
  comp: shallow(<AddCake {...props} />)
});

test('it renders the header', t => {
  const props = {
    history: { history: true }
  };
  const { comp } = createComp(props);
  const header = comp.find(Header);
  t.is(header.props().title, 'Add Cake');
});

test('renders add cake form container', t => {
  const props = {
    history: { history: true }
  };
  const { comp } = createComp(props);
  const addCakeFormContainer = comp.find(AddCakeForm);
  t.truthy(addCakeFormContainer.exists());
  t.truthy(addCakeFormContainer.props().history, props.history);
});
