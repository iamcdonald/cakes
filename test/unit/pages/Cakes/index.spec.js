import React from 'react';
import { shallow } from 'enzyme';
import test from 'ava';
import Cakes from '../../../../src/pages/Cakes';
import { Header } from '../../../../src/components';
import { CakeList } from '../../../../src/containers';

const createComp = () => ({
  comp: shallow(<Cakes />)
});

test('it renders the header', t => {
  const { comp } = createComp();
  const header = comp.find(Header);
  t.is(header.props().title, 'Cakes');
  t.deepEqual(header.props().link, { path: '/add', text: 'add a cake' });
});

test('it renders a CakeList container', t => {
  const { comp } = createComp();
  t.truthy(comp.find(CakeList).exists());
});
