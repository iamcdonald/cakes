const BASE_URL =
  'http://ec2-52-209-201-89.eu-west-1.compute.amazonaws.com:5000/api/cakes';

const getAllSuccess = (cy, delay) => {
  return cy.fixture('cakes').then(json =>
    cy
      .route({
        method: 'GET',
        url: BASE_URL,
        delay: delay,
        status: 200,
        response: json
      })
      .then(_ => json)
  );
};

const getAllFailure = (cy, delay) => {
  return cy.route({
    method: 'GET',
    url: BASE_URL,
    delay: delay,
    status: 500,
    response: ''
  });
};

const CakesService = {
  _init: function(cy) {
    this._cy = cy;
    this._cy.server();
  },
  getAll: function({ success = true, delay = 0 }) {
    if (success) {
      return getAllSuccess(this._cy, delay);
    }
    return getAllFailure(this._cy, delay);
  },
  add: function({ success = true, delay = 0, cake = {} }) {
    const newCake = Object.assign({}, cake, { id: 'asgdhjasd' });
    return cy
      .route({
        method: 'POST',
        url: BASE_URL,
        delay: delay,
        status: success ? 200 : 500,
        response: success ? newCake : ''
      })
      .then(_ => newCake);
  }
};

const create = cy => {
  const cs = Object.create(CakesService);
  cs._init(cy);
  return cs;
};

module.exports = {
  create
};
