module.exports = {
  AddCake: require('./add-cake'),
  Cakes: require('./cakes'),
  CakeDetail: require('./cake-detail')
};
