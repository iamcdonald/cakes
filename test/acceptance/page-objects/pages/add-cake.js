const { AddCakeForm, ErrorBox } = require('../components');

const AddCake = {
  _init: function(cy, ctx) {
    this._cy = cy;
    this._selector = `${ctx} [data-test=add-cake-page]`;
    this.form = AddCakeForm.create(this._cy, this._selector);
    this.serviceError = ErrorBox.create(
      this._cy,
      this._selector,
      'Failed to add cake, please try again.'
    );
    this.validationError = ErrorBox.create(
      this._cy,
      this._selector,
      'All fields are required.'
    );
  },
  isVisible: function() {
    this._cy.get(`${this._selector}`);
  },
  showsFormInvalidError: function() {
    this.validationError.isVisible();
  },
  showsServiceError: function() {
    this.serviceError.isVisible();
  }
};

const create = (cy, ctx) => {
  const addCakePage = Object.create(AddCake);
  addCakePage._init(cy, ctx);
  return addCakePage;
};

module.exports = {
  create
};
