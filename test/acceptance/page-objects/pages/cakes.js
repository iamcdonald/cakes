const { Loader, ErrorBox, CakeList } = require('../components');

const Cakes = {
  _init: function(cy, ctx) {
    this._cy = cy;
    this._selector = `${ctx} [data-test=cakes-page]`;
    this.loader = Loader.create(this._cy, this._selector);
    this.loadingError = ErrorBox.create(
      this._cy,
      this._selector,
      'An error has occurred fetching the cakes.'
    );
    this.list = CakeList.create(this._cy, this._selector);
  },
  isVisible: function() {
    this._cy.get(`${this._selector}`);
  },
  isLoading: function() {
    this.loader.isVisible();
  },
  showsLoadingError: function() {
    this.loadingError.isVisible();
  },
  navigateToAddCakePage: function() {
    this._cy.get(`${this._selector} [data-test='header-link']`).click();
  },
  clickCake: function(cake) {
    this.list.clickCake(cake);
  }
};

const create = (cy, ctx) => {
  const cakesPage = Object.create(Cakes);
  cakesPage._init(cy, ctx);
  return cakesPage;
};

module.exports = {
  create
};
