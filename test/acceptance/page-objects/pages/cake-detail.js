const CakeDetail = {
  _init: function(cy, ctx) {
    this._cy = cy;
    this._selector = `${ctx} [data-test=cake-detail-page]`;
  },
  isVisible: function() {
    this._cy.get(`${this._selector}`);
  },
  hasName: function(name) {
    this._cy.get(`${this._selector} dd`).contains(name);
  },
  hasComment: function(comment) {
    this._cy.get(`${this._selector} dd`).contains(comment);
  },
  hasImage: function(imageURL) {
    this._cy.get(`${this._selector} img[src='${imageURL}']`);
  },
  hasYumFactor: function(value) {
    this._cy.get(`${this._selector} dd`).contains(value);
  },
  showsCake: function(cake) {
    this.hasName(cake.name);
    this.hasComment(cake.comment);
    this.hasImage(cake.imageUrl);
    this.hasYumFactor(cake.yumFactor);
  },
  navigateToCakesPage: function() {
    this._cy.get(`${this._selector} [data-test='header-link']`).click();
  }
};

const create = (cy, ctx) => {
  const cd = Object.create(CakeDetail);
  cd._init(cy, ctx);
  return cd;
};

module.exports = {
  create
};
