const CakeItem = require('./cake-item');

const CakeList = {
  _init: function(cy, ctx) {
    this._cy = cy;
    this._selector = `${ctx} [data-test=cake-list]`;
  },
  isVisible: function() {
    this._cy.get(this._selector);
  },
  hasCakes: function(cakes) {
    cakes.forEach(cake => {
      const ci = CakeItem.create(this._cy, this._selector, cake);
      ci.matches();
    });
  },
  hasCake: function(cake) {
    this.hasCakes([cake]);
  },
  clickCake: function(cake) {
    const ci = CakeItem.create(this._cy, this._selector, cake);
    ci.showDetail();
  }
};

const create = (cy, ctx) => {
  const cl = Object.create(CakeList);
  cl._init(cy, ctx);
  return cl;
};

module.exports = {
  create
};
