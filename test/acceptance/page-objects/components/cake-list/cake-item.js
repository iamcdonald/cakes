const selector = '[data-test=cake-item]';

const CakeItem = {
  _init: function(cy, ctx, cake) {
    this._cy = cy;
    this._cake = cake;
    this._selector = `${ctx} [data-test=cake-item-${cake.id}]`;
  },
  matches: function() {
    this.hasName(this._cake.name);
    this.hasImage(this._cake.imageUrl);
  },
  hasName: function(name) {
    this._cy.get(`${this._selector} span`).contains(name);
  },
  hasImage: function(imageUrl) {
    this._cy.get(`${this._selector} img`).should('have.attr', 'src', imageUrl);
  },
  showDetail: function() {
    this._cy.get(this._selector).click();
  }
};

const create = (cy, ctx, idx) => {
  const ci = Object.create(CakeItem);
  ci._init(cy, ctx, idx);
  return ci;
};

module.exports = {
  create,
  selector: selector
};
