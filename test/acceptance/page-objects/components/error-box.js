const ErrorBox = {
  _init: function(cy, ctx, message) {
    this._cy = cy;
    this._message = message;
    this._selector = `${ctx} [data-test=error-box]`;
  },
  isVisible: function() {
    this._cy.get(this._selector).contains(this._message);
  }
};

const create = (cy, ctx, message) => {
  const eb = Object.create(ErrorBox);
  eb._init(cy, ctx, message);
  return eb;
};

module.exports = {
  create
};
