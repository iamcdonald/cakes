const AddCakeForm = {
  _init: function(cy, ctx) {
    this._cy = cy;
    this._selector = `${ctx} [data-test=add-cake-form]`;
  },
  isVisible: function() {
    this._cy.get(`${this._selector}`);
  },
  cancel: function() {
    this._cy.get(`${this._selector} [data-test='cancel-button']`).click();
  },
  submit: function() {
    this._cy.get(`${this._selector}`).submit();
  },
  setName: function(name) {
    this._cy.get(`${this._selector} input[name='name']`).type(name);
  },
  setComment: function(comment) {
    this._cy.get(`${this._selector} input[name='comment']`).type(comment);
  },
  setImageURL: function(imageURL) {
    this._cy.get(`${this._selector} input[name='imageUrl']`).type(imageURL);
  },
  setYumFactor: function(value) {
    this._cy
      .get(`${this._selector} input[name='yums'][value=${value}]`)
      .click();
  },
  addCake: function(cake) {
    this.setName(cake.name);
    this.setComment(cake.comment);
    this.setImageURL(cake.imageUrl);
    this.setYumFactor(cake.yumFactor);
  }
};

const create = (cy, ctx) => {
  const acf = Object.create(AddCakeForm);
  acf._init(cy, ctx);
  return acf;
};

module.exports = {
  create
};
