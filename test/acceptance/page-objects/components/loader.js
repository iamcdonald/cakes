const Loader = {
  _init: function(cy, ctx) {
    this._cy = cy;
    this._selector = `${ctx} [data-test=loader]`;
  },
  isVisible: function() {
    this._cy.get(this._selector);
  }
};

const create = (cy, ctx) => {
  const lo = Object.create(Loader);
  lo._init(cy, ctx);
  return lo;
};

module.exports = {
  create
};
