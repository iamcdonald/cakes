module.exports = {
  AddCakeForm: require('./add-cake-form'),
  ErrorBox: require('./error-box'),
  Loader: require('./loader'),
  CakeList: require('./cake-list')
};
