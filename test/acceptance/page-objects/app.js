const { Cakes, AddCake, CakeDetail } = require('./pages');

const App = {
  _init: function(cy, url) {
    this._cy = cy;
    this._url = url;
    this._selector = '[data-test=app]';
    this.pages = {
      cakes: Cakes.create(this._cy, this._selector),
      addCake: AddCake.create(this._cy, this._selector),
      cakeDetail: CakeDetail.create(this._cy, this._selector)
    };
  },
  visit: function() {
    this._cy.visit(this._url);
  }
};

const create = (cy, url) => {
  const appPage = Object.create(App);
  appPage._init(cy, url);
  return appPage;
};

module.exports = {
  create
};
