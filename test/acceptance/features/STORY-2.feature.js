const CakeService = require('../stubs/cakes-service');
const App = require('../page-objects/app');

describe('STORY-2', () => {
  let app;
  let cs;
  beforeEach(() => {
    app = App.create(cy, 'http://localhost:8080');
    cs = CakeService.create(cy);
    cs.getAll({ success: true });
    app.visit();
  });

  describe('when on cakes page', () => {
    it('can navigate to add cakes page', () => {
      app.pages.cakes.isVisible();
      app.pages.cakes.navigateToAddCakePage();
      app.pages.addCake.isVisible();
    });
  });

  describe('when on add cake page', () => {
    describe('when cancel button pressed', () => {
      it('navigates to cakes page', () => {
        app.pages.cakes.isVisible();
        app.pages.cakes.navigateToAddCakePage();
        app.pages.addCake.isVisible();
        app.pages.addCake.form.cancel();
        app.pages.cakes.isVisible();
      });
    });

    describe('when submit button is pressed', () => {
      describe('when cake is invalid', () => {
        it('shows error message', () => {
          app.pages.cakes.isVisible();
          app.pages.cakes.navigateToAddCakePage();
          app.pages.addCake.isVisible();
          app.pages.addCake.form.submit();
          app.pages.addCake.showsFormInvalidError();
        });
      });

      describe('when cake is valid', () => {
        describe('when cake creation successful', () => {
          it('creates cake and adds it too cake list then navigates to cakes page', () => {
            app.pages.cakes.isVisible();
            app.pages.cakes.navigateToAddCakePage();
            app.pages.addCake.isVisible();
            const cake = {
              name: 'a cakey name',
              comment: 'particuarly cakey',
              imageUrl: 'a-url',
              yumFactor: 3
            };
            app.pages.addCake.form.addCake(cake);
            cs.add({ cake }).then(newCake => {
              app.pages.addCake.form.submit();
              app.pages.cakes.isVisible();
              app.pages.cakes.list.hasCake(newCake);
            });
          });
        });

        describe('when cake creation unsuccessful', () => {
          it('shows error message', () => {
            app.pages.cakes.isVisible();
            app.pages.cakes.navigateToAddCakePage();
            app.pages.addCake.isVisible();
            const cake = {
              name: 'a cakey name',
              comment: 'particuarly cakey',
              imageUrl: 'a-url',
              yumFactor: 3
            };
            app.pages.addCake.form.addCake(cake);
            cs.add({ success: false });
            app.pages.addCake.form.submit();
            app.pages.addCake.showsServiceError();
          });
        });
      });
    });
  });
});
