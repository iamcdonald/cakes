const CakeService = require('../stubs/cakes-service');
const App = require('../page-objects/app');

describe('STORY-3', () => {
  let app;
  let cs;
  beforeEach(() => {
    app = App.create(cy, 'http://localhost:8080');
    cs = CakeService.create(cy);
  });

  describe('when on cakes page', () => {
    let cakes;
    beforeEach(() => {
      return cs.getAll({ success: true }).then(c => {
        app.visit();
        cakes = c;
      });
    });

    describe('when clicking a cake', () => {
      it('shows loading message to user', () => {
        app.pages.cakes.clickCake(cakes[3]);
        app.pages.cakeDetail.isVisible();
        app.pages.cakeDetail.showsCake(cakes[3]);
      });

      describe("when clicking 'view all cakes' link", () => {
        beforeEach(() => {
          app.pages.cakes.clickCake(cakes[3]);
          app.pages.cakeDetail.isVisible();
        });

        it('navigates to cakes page', () => {
          app.pages.cakeDetail.navigateToCakesPage();
          app.pages.cakes.isVisible();
        });
      });
    });
  });
});
