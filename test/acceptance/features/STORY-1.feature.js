const CakeService = require('../stubs/cakes-service');
const App = require('../page-objects/app');

describe('STORY-1', () => {
  let app;
  let cs;
  beforeEach(() => {
    app = App.create(cy, 'http://localhost:8080');
    cs = CakeService.create(cy);
  });

  describe('when visiting app', () => {
    it('shows cakes page', () => {
      cs.getAll({ success: true });
      app.visit();
      app.pages.cakes.isVisible();
    });

    describe('when cakes are loading', () => {
      beforeEach(() => {
        cs.getAll({ success: true, delay: 2000 });
      });

      it('shows loading message to user', () => {
        app.visit();
        app.pages.cakes.isLoading();
      });
    });

    describe('when cakes cannot be retrieved', () => {
      beforeEach(() => {
        cs.getAll({ success: false });
      });
      it('shows error message to user', function() {
        app.visit();
        app.pages.cakes.showsLoadingError();
      });
    });

    describe('when cakes are successfully retrieved', () => {
      let cakes;
      beforeEach(() => {
        return cs.getAll({ success: true }).then(response => {
          cakes = response;
        });
      });

      it('shows populated cake list to user', () => {
        app.visit();
        app.pages.cakes.list.hasCakes(cakes);
      });
    });
  });
});
